# GitLab Job Application Process
### Sample Application

_Please write a small Go-based application that will get HTTP response times over 5 minutes from your location to https://gitlab.com. Please make sure that your code is testable, uses all best Go-lang practices in terms of code guidelines, but also has a CI pipeline. You are free to use whatever CI tool you prefer._