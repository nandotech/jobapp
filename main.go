package main

import (
	"io/ioutil"
	"log"
	"net"
	"time"
)

func main() {

	conn, err := net.Dial("tcp", "gitlab.com:80")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	conn.Write([]byte("GET / HTTP/1.0\r\n\r\n"))

	start := time.Now()

	_, err = ioutil.ReadAll(conn)
	if err != nil {
		panic(err)
	}

	log.Println("Full Response Time: ", time.Since(start))
}
