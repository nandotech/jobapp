**Question 2 (Go)** _Do you need both environment variables GOPATH / GOROOT? Why?_

**Question 3 (Go)** _How would you implement concurrent and blocking stack and queue in Go?_

**Other general questions related to your candidacy (required)**

**Question 1 (general)** _What do you do to increase your and your teammates productivity as a developer? Keep in mind that you are applying as CI/CD engineer._

**Question 2 (general)** _You received an implementation of a feature proposal and you see that the author has spent a significant amount of time on it. You don't agree with the way it was implemented and designed. How do you approach this situation, and how do you communicate with the contributor?_

**Question 3 (general)** _Do you have any remote working experience? What are pros and cons of working remotely?_

**Question 4 (general)** _Please describe your CI/CD experience._

**Question 5 (general)** - _Do you have an open source project that you own or contributed to that you feel particularly proud about?_